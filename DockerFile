FROM node:4.4.3-slim

WORKDIR /app
ADD package.json /app/
RUN npm config set proxy http://ancy.proxy.corp.sopra:8080/
RUN npm install
ADD app.js /app/
EXPOSE 10210
ENV API_HOST localhost
ENV API_PORT 10210

ENTRYPOINT ["node", "app.js"]